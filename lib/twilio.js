const accountSid = process.env.TWILIO_ACCOUNT_SID; // Your Account SID from www.twilio.com/console
const authToken  = process.env.TWILIO_AUTH_TOKEN;   // Your Auth Token from www.twilio.com/console

const toNumber   = process.env.PHONE_TO
const fromNumber = process.env.PHONE_FROM
 
const client = require('twilio')(accountSid, authToken);

const sendMessage = (text) => {
    client.messages.create({
        body: text,
        to: toNumber,  
        from: fromNumber 
    })
    .then((message) => console.log('Message sent: ', message.sid));    
}

module.exports = {
    sendMessage
}