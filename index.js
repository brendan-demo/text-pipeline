#!/usr/bin/env node

require('dotenv').config();

const twilio = require('./lib/twilio');

if (process.argv.length < 3) {
    console.error('No message specified!')
    process.exit(2);
}

const msg = process.argv.slice(2)
twilio.sendMessage(msg);
